import "../Styles/navbar_style.css"

export default () => {
    return (
        <>
            <nav>
                <a href="/">Home</a>
                <a href="/cv">CV</a>
                <a href="/contact">Contact</a>
            </nav>
        </>
        )
}