import { Routes, Route, A } from '@solidjs/router';
import type { Component } from 'solid-js';
import Contact from './client/Components/Contact';
import CV from './client/Components/CV';
import Homepage from './client/Components/Homepage';
import NavBar from './client/Components/NavBar';
import Footer from './client/Components/Footer';
import "./client/Styles/app_style.css";
/*
Front page
TODO:
 - CV
 - Contact
 - Static Background
*/

const App: Component = () => {
  return (
    <>
      <div id="staticBG">  
        <h1>Vetle Skaar</h1>
        <NavBar />
      </div>
      <Routes>
        <Route path={"/"} component={Homepage} />
        <Route path={"/cv"} component={CV} />
        <Route path={"/contact"} component={Contact} />
      </Routes>
      <Footer />
    </>
    
  );
};

export default App;
